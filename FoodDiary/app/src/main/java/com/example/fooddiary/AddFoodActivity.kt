package com.example.fooddiary

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_food.*

class AddFoodActivity : AppCompatActivity() {

    companion object {
        val FOOD_ITEM_KEY = "FOOD_ITEM_KEY"
        val NAME_KEY = "NAME_KEY"
        val CALORIES_KEY = "CALORIES_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_food)
    }

    fun addFoodClicked(view: View) {
        if (isValidInput()) {
            val result = Intent()
            val bundle = Bundle()
            bundle.putString(NAME_KEY, foodNameTxt.text.toString())
            bundle.putInt(CALORIES_KEY, addCaloriesTxt.text.toString().toInt())
            result.putExtra(FOOD_ITEM_KEY, bundle)
            setResult(Activity.RESULT_OK, result)
            finish()
        }
        else {
            Toast.makeText(applicationContext,"Please enter a valid name and calorie total.", Toast.LENGTH_LONG).show()
        }
    }

    private fun isValidInput(): Boolean {
        var isValid = true;
        if (foodNameTxt.text.isNullOrBlank()) {
            foodNameTxt.error = getString(R.string.empty_name_error_msg)
            isValid = false
        }
        if (addCaloriesTxt.text.isNullOrBlank()) {
            addCaloriesTxt.error = getString(R.string.empty_cals_error_msg)
            isValid = false
        }
        else {
            try {
                var calories = addCaloriesTxt.text.toString().toInt()
                if (calories < 0) {
                    addCaloriesTxt.error = getString(R.string.negative_cals_error_msg)
                    isValid = false
                }
            }
            catch (e: IllegalArgumentException) {
                addCaloriesTxt.error = getString(R.string.invalid_cal_input_error_msg)
                isValid = false
            }
        }

        return isValid;
    }
}
