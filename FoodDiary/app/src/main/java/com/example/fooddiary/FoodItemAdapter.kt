package com.example.fooddiary

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class FoodItemAdapter(private val context: Context,
                      private val dataSource: ArrayList<FoodItem>) : BaseAdapter() {
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.food_item_row, parent, false)

        // Get food name element
        val foodNameView = rowView.findViewById(R.id.foodItemRowNameTxt) as TextView

        // Get food calorie element
        val foodCalView = rowView.findViewById(R.id.foodItemRowCalsTxt) as TextView

        val foodItem = getItem(position) as FoodItem

        foodNameView.text = foodItem.name
        foodCalView.text = foodItem.calories.toString()

        return rowView
    }
}