package com.example.fooddiary

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.app.AlertDialog
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.calc_tdee_dialog.*
import kotlinx.android.synthetic.main.calorie_input_dialog.*

const val NEW_FOOD_REQUEST = 1
const val LB_TO_KG: Double = 0.453592
const val IN_TO_CM: Double = 2.54

enum class Gender {
    MALE,
    FEMALE
}

enum class ActivityLevel {
    SEDENTARY,
    MODERATE,
    ATHLETIC
}

enum class WeightGoal {
    LOSE,
    MAINTAIN,
    GAIN
}

class MainActivity : AppCompatActivity() {

    private var goalTotalCalories: Int = 0
    private var remainingCalories: Int = 0
    private var totalCaloriesSum: Int = 0
    private var foodItemList: ArrayList<FoodItem> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup the add new food floating action button
        val addFoodItemFabView: View = findViewById(R.id.addFoodItemFab)
        addFoodItemFabView.setOnClickListener { view ->
            val intent = Intent(this@MainActivity, AddFoodActivity::class.java)
            startActivityForResult(intent, NEW_FOOD_REQUEST)
        }

        // Show the dialog for the user to input their total calories goal
        showTotalCalorieDialog("")

        // Setup the food item adapter
        val adapter = FoodItemAdapter(this, foodItemList)
        foodItemListView.adapter = adapter

        //val tdeeCal = calcTdee(Gender.MALE, 25, 165, 72, ActivityLevel.MODERATE, WeightGoal.MAINTAIN)
    }

    // Called when the add food activity returns
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEW_FOOD_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                // 3
                val bundle = data?.getBundleExtra(AddFoodActivity.FOOD_ITEM_KEY)
                if (bundle != null) {
                    val name = bundle.getString(AddFoodActivity.NAME_KEY)
                    val calories = bundle.getInt(AddFoodActivity.CALORIES_KEY)
                    foodItemList.add(FoodItem(name, calories))
                    // Need to notify the adapter the data set changed or else the app will crash
                    (foodItemListView.adapter as FoodItemAdapter).notifyDataSetChanged()
                    setRemainingCals(remainingCalories-calories)
                    calcAndSetTotalCalories()
                }
            }
        }
    }

    // Shows the total calorie dialog for users to input their desired calories
    // Users must either enter a valid calories number or press cancel to close the app
    private fun showTotalCalorieDialog(initialCalories: String) {
        val context = this
        val view = layoutInflater.inflate(R.layout.calorie_input_dialog, null)
        val dialog: AlertDialog = AlertDialog.Builder(context)
            .setTitle("Enter Total Calories")
            .setCancelable(false)
            .setView(view)
            .setPositiveButton("Accept", null)
            .setNegativeButton(android.R.string.cancel) { dialog, p1 ->
                Toast.makeText(applicationContext,getString(R.string.exiting_app_msg) , Toast.LENGTH_SHORT).show()
                finish()
            }
            .setNeutralButton(R.string.calculate_label) { dialog, p1 ->
                showCalcTdeeDialog()
                dialog.dismiss()
            }
            .create()


        val caloriesEditText = view.findViewById(R.id.totalCaloriesEditText) as EditText

        // Set the calories textbox if necessary
        if (initialCalories.isNullOrBlank() == false) {
            caloriesEditText.setText(initialCalories)
        }

        dialog.show()

        // Override the postive button so that the form won't close if there's invalid input
        // This way we can tell the user what fields are wrong without restarting the dialog
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {
                val totalCaloriesInput = caloriesEditText.text
                val isValid = isValidCalorieInput(totalCaloriesInput)
                val errorMessage = getString(R.string.validation_invalid_format)

                if (isValid) {
                    // Set the goal total calories and close the dialog
                    goalTotalCalories = totalCaloriesInput.toString().toInt()
                    setRemainingCals(goalTotalCalories)
                    calcAndSetTotalCalories()
                    dialog.dismiss()
                }
                else {
                    //  Tell the user what's wrong
                    Toast.makeText(applicationContext,errorMessage, Toast.LENGTH_LONG).show()
                    caloriesEditText.error = errorMessage
                }
            }
        })
    }

    private fun showCalcTdeeDialog() {
        val context = this
        val view = layoutInflater.inflate(R.layout.calc_tdee_dialog, null)
        val dialog: AlertDialog = AlertDialog.Builder(context)
            .setTitle("Calculate TDEE")
            .setCancelable(false)
            .setView(view)
            .setPositiveButton("Accept", null)
            .setNegativeButton(android.R.string.cancel) { dialog, p1 ->
                showTotalCalorieDialog("")
                dialog.cancel()
            }
            .create()

        // Get the form data from the view
        val genderRg = view.findViewById<RadioGroup>(R.id.rg_gender)
        val ageView = view.findViewById<EditText>(R.id.age)
        val weightView = view.findViewById<EditText>(R.id.weight)
        val heightView = view.findViewById<EditText>(R.id.height)
        val activityRg = view.findViewById<RadioGroup>(R.id.rg_activity)
        val goalRg = view.findViewById<RadioGroup>(R.id.rg_goal)

        dialog.show()

        // Override the postive button so that the form won't close if there's invalid input
        // This way we can tell the user what fields are wrong without restarting the dialog
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View) {
                // Sanitize inputs
                var isValid: Boolean = true
                var gender = Gender.MALE
                if (ageView.text.isNullOrBlank()) {
                    ageView.error = "Age cannot be empty"
                    isValid = false
                }
                if (weightView.text.isNullOrBlank()) {
                    weightView.error = "Weight cannot be empty"
                    isValid = false
                }
                if (heightView.text.isNullOrBlank()) {
                    heightView.error = "Height cannot be empty"
                    isValid = false
                }

                if (isValid) {
                    var age = ageView.text.toString().toInt()
                    var weight = weightView.text.toString().toInt()
                    var height = heightView.text.toString().toInt()
                    var activity = ActivityLevel.SEDENTARY
                    var goal = WeightGoal.MAINTAIN

                    when (genderRg.checkedRadioButtonId) {
                        R.id.rb_male -> gender = Gender.MALE
                        R.id.rb_female -> gender = Gender.FEMALE
                    }

                    when (activityRg.checkedRadioButtonId) {
                        R.id.rb_sedentary -> activity = ActivityLevel.SEDENTARY
                        R.id.rb_moderate -> activity = ActivityLevel.MODERATE
                        R.id.rb_active -> activity = ActivityLevel.ATHLETIC
                    }

                    when (goalRg.checkedRadioButtonId) {
                        R.id.rb_lose -> goal = WeightGoal.LOSE
                        R.id.rb_maintain -> goal = WeightGoal.MAINTAIN
                        R.id.rb_gain -> goal = WeightGoal.GAIN
                    }

                    val tdee: Int = calcTdee(gender, age, weight, height, activity, goal)

                    showTotalCalorieDialog(tdee.toString())
                    dialog.dismiss()
                }
                else {
                    //  Tell the user what's wrong
                    Toast.makeText(applicationContext,"Invalid input!", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    // Gets whether the text in an Editable field is a valid calorie input or not
    private fun isValidCalorieInput(calorieInput: Editable) : Boolean {
        var isValid = true
        if (calorieInput.isBlank()) {
            isValid = false
        }
        else {
            try {
                var calories = calorieInput.toString().toInt()
                if (calories < 0) {
                    isValid = false
                }
            }
            catch (e: IllegalArgumentException) {
                isValid = false
            }
        }

        return isValid
    }

    // Sets the remaining calories variable and sets the UI label properly
    private fun setRemainingCals(remainingCals: Int) {
        remainingCalories = remainingCals
        caloriesRemainingTxt.text = getString(R.string.calories_remaining) + " " + remainingCalories
        if (remainingCalories < 0) {
            caloriesRemainingTxt.setTextColor(ContextCompat.getColor(this, R.color.negativeCalsColor));
        }
    }

    // Calculates the total number of calories eaten and sets the UI label properly
    private fun calcAndSetTotalCalories() {
        totalCaloriesSum = 0
        for (foodItem in foodItemList) {
            totalCaloriesSum += foodItem.calories
        }
        totalCalorieSumTxt.text = getString(R.string.total_calories_label) + " " + totalCaloriesSum
    }

    // Calculate Total Daily Energy Expenditure using the Mifflin-St Jeor Formula
    // (https://www.freedieting.com/calorie-needs)
    private fun calcTdee(gender: Gender, age: Int, weightLb: Int, heightIn: Int, activityLevel: ActivityLevel, goal: WeightGoal) : Int {
        var genderFactor: Int = 0
        var activityFactor: Double = 0.0
        var goalFactor = 0

        // Set the gender factor based on male or female
        when (gender) {
            Gender.MALE -> genderFactor = 5
            Gender.FEMALE -> genderFactor = -161
        }

        // Convert the weight to kg
        val weightKg = weightLb * LB_TO_KG

        // Convert height to cm
        val heightCm = heightIn * IN_TO_CM

        // Set the activity factor based on activity level
        when (activityLevel) {
            ActivityLevel.SEDENTARY -> activityFactor = 1.2
            ActivityLevel.MODERATE -> activityFactor = 1.55
            ActivityLevel.ATHLETIC -> activityFactor = 1.725
        }

        // Set the goal factor based on what the user wants to do
        when (goal) {
            WeightGoal.LOSE -> goalFactor = -500
            WeightGoal.MAINTAIN -> goalFactor = 0
            WeightGoal.GAIN -> goalFactor = 500
        }

        // Calculate TDEE
        // 10 x weight (kg) + 6.25 x height (cm) – 5 x age (y) + genderFactor

        val tdeeCalc: Int = ((10 * weightKg + 6.25 * heightCm - 5 * age + genderFactor) * activityFactor).toInt() + goalFactor

        return tdeeCalc
    }


}
