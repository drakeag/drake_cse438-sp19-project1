In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

Alex Drake (drakeag) Project 1
2/7/2019

Tip

When copying this project from my dev location to the repo, I had to do the following
on the new project to get it to run properly again:
    1. Build -> Clean Project
    2. Build -> Rebuild Project

Creative Portion

For my creative portion I decided to implement a Total Daily Energy Expenditure (TDEE)
calculator based upon the calorie goals of the user. TDEE is an estimate of how many 
calories you burn per day. On startup instead of entering a total calorie goal manually,
the user has the option to click "Calculate TDEE". Clicking this button will pop up a 
dialog asking basic information about the user. After clicking submit the user will be 
redirected to the original Total Calorie dialog with the calculated TDEE autofilled. 
The user can then accept this calorie count or adjust it as they please.

I chose this feature because it is a common feature on calorie counting apps. People
are typically counting their calories for a specific weight goal. Having a TDEE
calculator takes the guess work out of selecting the appropriate calories for their
goals.

When the user clicks "Calculate TDEE" a new dialog pops up. The dialog itself is
a simple AlertDialog that loads a layout with the necessary form information. The
information collected is: gender, age, weight, height, activity level, and
weight goal. Gender, activity level, and weight goal are all radio boxes so the
user can easily select them. Age, weight, and height are manually entered. Once
the user clicks "Submit" the app uses the Mifflin-St Jeor Formula to calculate
the user's TDEE. This formula is listed below.

Men
10 x weight (kg) + 6.25 x height (cm) � 5 x age (y) + 5
Women
10 x weight (kg) + 6.25 x height (cm) � 5 x age (y) � 161

Source: https://www.freedieting.com/calorie-needs

1. (10 / 10 Points) User can enter total calorie amount on start up
2. (8 / 8 Points) User can add new food item by name
3. (8 / 8 Points) User can add new food item by calorie
4. (4 / 4 Points) Adding new food items is done in a second activity
5. (5 / 5 Points) Calories remaining is updated with each new food item
6. (5 / 5 Points) Calorie consumed is updated with each new food item
7. (10 / 10 Points) The list of food items displays foods and their respective calories amounts
8. (10 / 10 Points) Color change when calorie count becomes negative
9. (10 / 10 Points) All inputs are filtered and error messages are displayed accordingly
10. (2 / 2 Points) Code is clean and commented
11. (3 / 3 Points) App is visually appealing
12. (15 / 15 Points) Creative portion - design your own feature(s)!

Total: 90 / 90

Great job! I loved your creative portion, even though it told me that I need to start eating a lot less (sad but true =( ).